provider "aws" {
  region = "us-west-1"
}

resource "aws_instance" "xwiki" {
  ami           = "ami-0bdb828fd58c52235"
  instance_type = "t2.micro"

  subnet_id              = "subnet-70f5d72b"
  vpc_security_group_ids = ["sg-33648048"]

  connection {
    Type        = "ssh"
    private_key = "${file("/home/mehul/.ssh/docker-test")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo yum install -y docker",
      "sudo service docker restart",
      "wget -O docker-compose.yml https://raw.githubusercontent.com/xwikicontrib/docker-xwiki/master/docker-compose-postgres.yml",
      "sudo docker-compose up -d",
    ]
  }
}
